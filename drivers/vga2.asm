    BITS 16

    jmp routing_procs

call_init_display_800_600:
    call init_display_800_600
    jmp exit_driver

call_get_addr_display:
    call get_addr_display
    jmp exit_driver

call_do_blank_screen:
    call do_blank_screen
    jmp exit_driver

routing_procs:

    ; AX=0h - INIT DISPLAY, returns nothing
    cmp ax,0
    je call_init_display_800_600
    ; AX=1h - CHECKS DISPLAY ADDRESS, returns address in ebx
    cmp ax,1
    je get_addr_display

    ; AX=2h,EBX=Addr of Display,DH=COLOR - do blank screen, returns nothing
    cmp ax,2
    je call_do_blank_screen

exit_driver:
    iret

init_display_800_600:
    push bx    
    push ax
    mov ax,04F02h
    ;mov bx,103h
    ;or  bx,0010000000000000b
    mov bx,2103h    
    int 10h
    pop ax
    pop bx
    ret

get_addr_display:
    push es
    push ax
    push di
    push cx

    mov ax,0A000h
    mov es,ax

    xor di,di
    mov ax,4F01h
    mov cx,103h    
    int 10h

    mov di,40 ; we will take the address out of the info
    mov ebx, dword [es:di]

    pop cx
    pop di    
    pop ax    
    pop es    
    ret

do_blank_screen:

    push ecx
    push ebx
    mov ecx,480000
    loop_write_pixels:
        mov byte [ebx],dh
        inc ebx
        loop loop_write_pixels
    

    pop ebx    
    pop ecx
    ret

print_letter:
        

    ret

