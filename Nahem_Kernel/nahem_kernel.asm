BITS 32
section	.text
   global _start     ;must be declared for linker (ld)
	
_start:	            ;tells linker entry point
   jmp $  

section	.data
msg db 'Hello, world!', 0  ;string to be printed
