; This Bootloader is intended to test various things before loading kernel:
; 1. Simple graphics mode enabled? Do it (80x25)
; 2. Test architecture. System is allowed to boot only on i586 and above!
; 3. A20 line enabled?
; 4. Preparing IMS (Info Memory Segment) for kernel usage (multiboot specs)
; 5. Mapping Memory allocated and store in IMS
; 6. Mapping linear video buffer, store in IMS
; 7. Sending all the information to an allocated memory segment called IMS 
; 8. Load the kernel executable after every test with paramters about IMS address...
; 9. Pass control to kernel

stage1:
	;; We are loaded at 0x7000:0000
	mov ax,0x7000
	mov ds,ax
;; Let's initialize the screen with the BIOS!
	mov ax,03
	int 10h
	mov ax,0B800h
	mov fs,ax
jmp $
;; Stage 2 - Testing Architecture (Using CPUID availability checking)
;; In the odd chance, someone tries to load the system on i486 and below, the bootloader will
;; stop it right here! I assume here that nobody will try to run it on some crude i286 system or below... 
;; (Unless you are a junker with a freaking old system, in which in this case - go away to your old DOS... sorry!)

	jmp testing_arch ; skip some local procedures (e.g. printing a string and clearing the screen)

;; 
; Function: print_message
; Parameters: Offset of message, terminated by 0 (C-style string)
; Purpose: Print message on screen
; Returns: nothing
	error_arch_msg db "Your system is too old for us... We require at least i586 (Pentium) for loading our crappy system!",0
	success_arch_msg db "Your system is using an i586 processor or above! Proceeding!",0
	error_a20line_msg db "Can't enable A20 Line!",0
	success_a20line_msg db "A20 Line is enabled!",0
print_message:
	push bx
	push ax
	push cx
	push si

	lea si, [error_arch_msg]
	mov ah, 08h
	
	loop_print_msg:
		mov al,[si]
		cmp al,0
		jz end_printing_arch_error
		mov [fs:bx],ax	
		inc si
		add bx,2
		jmp loop_print_msg


	end_printing_arch_error:

	pop si
	pop cx
	pop ax
	pop bx
	jmp $	
	ret ; actually, it will never return... putting it for sake of procedure closing (a bad habit I guess)
;; 
; Function: init_screen
; Purpose: Init_screen procedure, called for clearing screen entirely (in the good old way)...
; Returns: nothing
init_screen:
	push bx
	push ax
	push cx
	xor bx,bx
	mov cx,2000
	loop_cls:
		mov word [fs:bx],0
		add bx,2		
		loop loop_cls
	pop cx
	pop ax
	pop bx	
	ret



; Function: testing_cpuid_available
; Purpose: Test CPUID availability
; Returns: EAX with various values, 0 = CPUID isn't supported natively (e.g. not i586 or above), else = CPUID is supported
testing_cpuid_available:

	pushfd                               ;Save EFLAGS
	pushfd                               ;Store EFLAGS
	xor dword [esp],0x00200000           ;Invert the ID bit in stored EFLAGS
	popfd                                ;Load stored EFLAGS (with ID bit inverted)
	pushfd                               ;Store EFLAGS again (ID bit may or may not be inverted)
	pop eax                              ;eax = modified EFLAGS (ID bit may or may not be inverted)
	xor eax,[esp]                        ;eax = whichever bits were changed
	popfd                                ;Restore original EFLAGS
	and eax,0x00200000                   ;eax = zero if ID bit can't be changed, else non-zero
	ret

testing_arch: ;; the fun part is starting actually here!

	call init_screen ; ya want a clean screen, don't you?
	call testing_cpuid_available ; testing CPUID availability
	cmp eax,0
	jnz stage3_a20 ; if eax is not 0, we passed this test and can proceed further.
	;call print_error_arch ; call this procedure if we can't boot...


;; Stage 3 - Enabling A20 Line
;; The approach will be very simple - we will try to test if A20 line is already enabled. If not so - we will try to enable it by ourselves by many techniques...
stage3_a20:

	;call print_good_arch
	jmp check_a20line_main ; go to the main checking a20 line

; Function: check_a20
; Purpose: to check the status of the a20 line in a completely self-contained state-preserving way.
;          The function can be modified as necessary by removing push's at the beginning and their
;          respective pop's at the end if complete self-containment is not required.
; Returns: 0 in ax if the a20 line is disabled (memory wraps around)
;          1 in ax if the a20 line is enabled (memory does not wrap around)
check_a20:
	pushf
	cli ; disabling interrupts
	push ds
	push es
	push di
	push si

	xor ax, ax
	mov es, ax
 
	not ax ; ax = 0xFFFF
	mov ds, ax
 
	mov di, 0x0500
	mov si, 0x0510
 
	mov al, byte [es:di]
	push ax
 
	mov al, byte [ds:si]
	push ax
 
	mov byte [es:di], 0x00
	mov byte [ds:si], 0xFF
 
	cmp byte [es:di], 0xFF
 
	pop ax

	mov byte [ds:si], al
 
	pop ax
	mov byte [es:di], al
 
	mov ax, 0
	je check_a20_exit
 
	mov ax, 1
 
check_a20_exit:
	pop si
	pop di
	pop es
	pop ds
	popf
	ret


; Function: enable_a20line
; Purpose: try to enable A20 Line first with BIOS interrupt and if it's not working - with the Keyboard controller. If failed - stop execution.
; Returns: nothing

enable_a20line:


	;; Try to enable A20 line with bios
	mov     ax,2403h                ;--- A20-Gate Support ---
	int     15h
	jb      a20_failed                  ;INT 15h is not supported
	cmp     ah,0
	jnz     a20_failed                  ;INT 15h is not supported
	 
	mov     ax,2402h                ;--- A20-Gate Status ---
	int     15h
	jb      a20_failed              ;couldn't get status
	cmp     ah,0
	jnz     a20_failed              ;couldn't get status
	 
	cmp     al,1
	jz      a20_activated           ;A20 is already activated
	 
	mov     ax,2401h                ;--- A20-Gate Activate ---
	int     15h
	jb      a20_failed             ;couldn't activate the gate
	cmp     ah,0
	jnz     a20_failed              ;couldn't activate the gate

	a20_failed:
	call enable_a20line_keyboard ; try to enable A20 line with keyboard controller
	call check_a20
	cmp ax,1 ; A20 line is activated
	jmp a20_activated
	
	a20_fault:
		;call print_error_a20
		jmp $

	a20_activated:	
	ret

; Function: enable_a20line
; Purpose: try to enable A20 Line with Keyboard controller.
; Returns: nothing

enable_a20line_keyboard:
 
        call    a20wait
        mov     al,0xAD
        out     0x64,al
 
        call    a20wait
        mov     al,0xD0
        out     0x64,al
 
        call    a20wait2
        in      al,0x60
        push    eax
 
        call    a20wait
        mov     al,0xD1
        out     0x64,al
 
        call    a20wait
        pop     eax
        or      al,2
        out     0x60,al
 
        call    a20wait
        mov     al,0xAE
        out     0x64,al
 
        call    a20wait
        sti
        ret
 
a20wait:
        in      al,0x64
        test    al,2
        jnz     a20wait
        ret
 
 
a20wait2:
        in      al,0x64
        test    al,1
        jz      a20wait2
        ret


check_a20line_main:
	jmp $
	call check_a20
	cmp ax,1 ; 0 = a20 line is disabled, 1 = enabled
	je stage4_ims ; proceed to next stage
	call enable_a20line ; otherwise enable a20line


;; Stage 4 - Preparing IMS (Info Memory Segment) for kernel usage
;; We will try to first check the amount of available memory.
;; Then calculating the optimised location for us to put the IMS and tables in it.
;; Example: If we have 2GB of RAM available, we will subtract about 100 Megabytes beyond that barrier
;; Then we will place the table there. for stability of kernel, we will implement another table like so
;; 7 bits reserved | IMS available? (1 bit) | 64 bits address (persumably, we will use now only 32 bit addresses) 
;; This data structure will be at 0x1000, physical address.
;; Kernel will try to find that table. If kernel didn't find any table (or first byte is 0)
;; Then kernel will assume that no IMS is available and will have to do every scanning by itself.
;; 
stage4_ims:
	;call print_success_a20 ; success with A20 line enabled - we want to inform the user about it.
	jmp $
	
	

;; Stage 5 - Enumarate PCI bus, checking for other usable hardware (primarily DMA/SATA and other usable controllers), store something in IMS

stage5_pci:

	jmp pci_try_finding_ide

; Function: check_ide_device
; Purpose: try to check if a specific device on a specific bus is an IDE controller
; Paramteres: AX = bus, BX = device
; Returns: CX in various states (1 = a IDE controller found, 0 = no IDE controller)
check_ide_device:
	
	ret	

;; starting finding a IDE controller
pci_try_finding_ide:
	

;; We have to find data mainly about SATA/IDE controllers
;; Therefore, We have to enumarate the PCI bus to find such devices.
;; In order to do that, we have to find some devices in Class code 0x01,
;; Then we check if subclass = 0x01 (IDE controller), or 0x05 (ATA controller) or 0x06 (Serial ATA Controller)

;; Stage 6 - Prepare protected mode
;; For supplying a modern environment, we have to enter a protected mode processor-state as soon as possible.
;; We have to setup some tables like GDT and others.

;; Stage 7 - Mapping usable graphical modes, optionally store in IMS
;; We want to map all usable graphical modes available (using VESA).

;; Stage 8 - Load the kernel executable.
;; We will use the IDE controller. We assume that the entire disk is formatted using FAT32.
;; Using the IDE controller and the FAT32 file system we will load the kernel executable

;; Stage 9 - Pass control to kernel
;; We will do last things here, and then pass control to our kernel.
;; This may include final checks and so before we are leaving this area...
;; Presumably here we will setup final preparations like actually entering protected mode.

	jmp $ ;; end here

times 8190-($-$$) db 0	; Pad remainder with 0s
dw 0xFF88